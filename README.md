# Weather App

### Preview
[https://weather-app-hljyonlmef.now.sh/](https://weather-app-hljyonlmef.now.sh/)

### Installing

```
git clone https://gitlab.com/vesic/weather-app
cd weather-app
npm install
npm start
open localhost:8080
```

### Built With

* React
* axios
* moment.js
* React Router