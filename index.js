import React from "react";
import ReactDOM from "react-dom";
import { AppContainer } from "react-hot-loader";
import Routes from './src/components/Routes';
import "./index.css";

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById("root")
  );
};

render(Routes);

// Webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept("./src/components/Routes", () => {
    render(Routes);
  });
}
