export const title = (str) => 
    str.replace(/\b\S/g, (t) => t.toUpperCase());