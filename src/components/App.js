import React, { Component } from "react";
import { Button, Grid, Row, Col } from "react-bootstrap";
import { Typeahead } from "react-bootstrap-typeahead";
import moment from "moment";
import axios from "axios";
import { OPEN_WEATHER_KEY } from "../../config";
import City from "./City";
import Search from "./Search";
import NoCity from "./NoCity";
import NavBar from "./NavBar";
import { CSSTransitionGroup } from "react-transition-group";

const WEATHER_API_URL = `https://api.openweathermap.org/data/2.5/weather?appid=${OPEN_WEATHER_KEY}&units=metric`;

class App extends Component {
  state = {
    selected: []
  };

  componentDidMount() {
    const selected = localStorage.getItem("@selected");
    if (selected) {
      this.setState({
        selected: JSON.parse(selected)
      });
    }
  }

  renderCities = () => {
    const { selected } = this.state;
    return selected.length ? (
      selected.map(city => (
        <City key={city.id} city={city} onRemoveCity={this.handleRemoveCity} />
      ))
    ) : (
      <NoCity />
    );
  };

  handleChange = selected => {
    if (selected.length > this.state.selected.length) {
      let city = selected[selected.length - 1];
      axios.get(`${WEATHER_API_URL}&q=${city.label}`).then(res => {
        city.data = res.data;
        city.time = moment().format("MMM Do");
        selected[selected.length - 1] = city;
        this.setState(
          {
            selected
          },
          () => this.setItem()
        );
      });
    } else {
      this.setState({ selected }, this.setItem());
    }
  };

  setItem = () => {
    localStorage.setItem("@selected", JSON.stringify(this.state.selected));
  };

  render() {
    return (
      <div>
        <NavBar />
        <Grid>
          <Row>
            <Col xs={12}>
              <Search
                selected={this.state.selected}
                handleChange={this.handleChange}
              />
            </Col>
          </Row>
          <Row>
            <CSSTransitionGroup
              transitionName="example"
              transitionEnterTimeout={200}
              transitionLeaveTimeout={200}
            >
              {this.renderCities()}
            </CSSTransitionGroup>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default App;
