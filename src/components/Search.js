import React, { Component } from "react";
import { Typeahead } from "react-bootstrap-typeahead";
import cities from "../cities";

class Search extends Component {
  render() {
    const { handleChange, selected } = this.props;
    return (
      <Typeahead
        clearButton
        onChange={selected => handleChange(selected)}
        multiple
        options={cities}
        placeholder="Choose a city..."
        selected={selected}
      />
    );
  }
}

export default Search;
