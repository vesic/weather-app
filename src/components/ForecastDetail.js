import React from "react";
import moment from "moment";

const ForecastDetail = ({ city }) => (
  <div style={styles.wrapper}>
    <div style={styles.detail}>
      <div>{moment(city.dt_txt).format("ddd Do HH:mm")}</div>
      <div>
        <img
          alt="weather-icon"
          src={`https://openweathermap.org/img/w/${city.weather[0].icon}.png`}
        />
      </div>
      <div>
        {`${Number.parseInt(city.main.temp_min)}\u2103`} -{" "}
        {`${Number.parseInt(city.main.temp_max)}\u2103`}
      </div>
      <div>Humidity {city.main.humidity}</div>
      <div>Wind {city.wind.speed}KM/h</div>
    </div>
  </div>
);

const styles = {
  detail: {
    width: 150,
    margin: 10,
    textAlign: "center",
    color: "rgba(0,0,0,0.6)",
    fontWeight: "bold"
  },
  wrapper: {
    background: "#fff",
    borderRadius: "2px",
    marginBottom: 10,
    marginRight: 10,
    width: 200,
    boxShadow: "1px 0px 5px 0px rgba(0,0,0,0.3)"
  }
};

export default ForecastDetail;
