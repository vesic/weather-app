import React, { Component } from "react";
import { Col } from "react-bootstrap";

class NoCity extends Component {
  state = {
    dots: ""
  };

  componentDidMount() {
    let i = 1;
    this.interval = setInterval(() => {
      if (i > 5) i = 1;
      let dots = "";
      for (let j = 0; j < i; j++) {
        dots += ".";
      }
      i += 1;
      this.setState({ dots });
    }, 1000);
  }

  render() {
    return (
      <Col xs={12} style={styles.main}>
        <h2>
          no city selected<span>{this.state.dots}</span>
        </h2>
      </Col>
    );
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
}

const styles = {
  main: {
    color: "rgba(0, 0, 0, .7)"
  }
};

export default NoCity;
