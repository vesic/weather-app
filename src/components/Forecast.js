import React, { Component } from "react";
import { Grid, Row } from "react-bootstrap";
import axios from "axios";
import moment from "moment";
import { OPEN_WEATHER_KEY } from "../../config";
import Map from "./Map";
import ForecastDetail from "./ForecastDetail";
import Loader from "./Loader";
import NavBar from "./NavBar";

const FORECAST_URL = `https://api.openweathermap.org/data/2.5/forecast?appid=${OPEN_WEATHER_KEY}&units=metric`;

class Forecast extends Component {
  state = {
    city: {},
    list: []
  };

  componentDidMount() {
    const { city } = this.props.match.params;
    axios.get(`${FORECAST_URL}&q=${city}`).then(res => {
      this.setState({
        city: res.data.city,
        list: res.data.list
      });
    });
  }

  renderForecast = () => {
    if (this.state.list.length > 0) {
      return this.state.list.map((city, i) => <ForecastDetail key={i} city={city} />);
    }
    return <Loader />;
  };

  render() {
    const { city } = this.props.match.params;
    return (
      <div>
        <NavBar />
        <Grid>
          <Row>
            <div style={styles.center}>{city} 5 days forecast</div>
          </Row>
          <Row>
            <div style={styles.forecast}>{this.renderForecast()}</div>
          </Row>
          <Row>
            <div style={styles.center}>{city} on map</div>
            {this.state.city.coord ? (
              <Map
                lat={this.state.city.coord.lat}
                lng={this.state.city.coord.lon}
              />
            ) : (
              <Loader />
            )}
          </Row>
        </Grid>
      </div>
    );
  }
}

const styles = {
  top: {
    marginTop: 10
  },
  forecast: {
    display: "flex",
    overflowX: "scroll",
    overflowY: "hidden"
  },
  center: {
    fontSize: "1.7em",
    margin: "0 0 10px 0",
  }
};

export default Forecast;
