import React, { Component } from "react";

class Map extends Component {
  componentDidMount() {
    const { lat, lng } = this.props;
    new window.google.maps.Map(this.map, {
      center: { lat, lng },
      zoom: 16,
      mapTypeId: "roadmap"
    });
  }

  render() {
    const mapStyle = {
      width: "100%",
      height: 300,
      margin: "0 auto"
    };
    return (
      <div>
        <div ref={map => (this.map = map)} style={mapStyle} />
      </div>
    );
  }
}

export default Map;
