import React from "react";
import { Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

const NavbarInstance = props => (
  <Navbar style={styles.navbar}>
    <Navbar.Header>
      <Navbar.Brand>
        <Link style={styles.link} to="/">
          Weather App
        </Link>
      </Navbar.Brand>
    </Navbar.Header>
  </Navbar>
);

const styles = {
  navbar: {
    background: "#1A2739"
  },
  link: {
    color: "#fff"
  }
};

export default NavbarInstance;
