import React, { Component } from "react";

class Loader extends Component {
  state = {
    dots: ""
  };

  componentDidMount() {
    let i = 1;
    this.interval = setInterval(() => {
      if (i > 5) i = 1;
      let dots = "";
      for (let j = 0; j < i; j++) {
        dots += ".";
      }
      i += 1;
      this.setState({ dots });
    }, 250);
  }

  render() {
    return (
      <div>
        <h1 style={styles.dots}>{this.state.dots}</h1>
      </div>
    );
  }

  componentWillMount() {
    clearInterval(this.interval);
  }
}

const styles = {
  dots: {
    fontSize: "5em"
  }
};

export default Loader;
