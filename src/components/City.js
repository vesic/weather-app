import React from "react";
import { Col } from "react-bootstrap";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { title } from "../util";

const Left = ({ label, time }) => (
  <div style={styles.left}>
    <div>{label}</div>
    <div>{time}</div>
  </div>
);

const Mid = ({ temp, desc }) => (
  <div style={styles.mid}>
    <div style={styles.temp}>{`${Number.parseInt(temp, 10)} \u2103`}</div>
    <div>{title(desc)}</div>
  </div>
);

const Right = ({ humidity, wind }) => (
  <div style={styles.right}>
    <div>Humidity {humidity}</div>
    <div>Wind {wind}</div>
  </div>
);

const City = ({ city }) => (
  <Col xs={12} sm={6} md={4} style={styles.main}>
    <Link style={styles.link} to={`/forecast/${city.label}`}>
      <div style={styles.city}>
        <Left {...city} />
        <Mid
          temp={city.data.main.temp}
          desc={city.data.weather[0].description}
        />
        <Right humidity={city.data.main.humidity} wind={city.data.wind.speed} />
      </div>
    </Link>
  </Col>
);

const styles = {
  main: {
    marginTop: 20
  },
  city: {
    backgroundColor: "#fff",
    padding: 20,
    margin: "10px 0",
    position: "relative",
    boxShadow: "5px 0 10px 0 rgba(0,0,0,0.3)",
    display: "flex",
    justifyContent: "space-around",
    borderRadius: "2px"
  },
  close: {
    position: "absolute",
    top: "0px",
    right: "0px",
    backgroundColor: "tomato",
    padding: 1
  },
  left: {
    textAlign: "center"
  },
  mid: {
    textAlign: "center"
  },
  right: {
    textAlign: "center"
  },
  temp: {
    fontSize: "2em"
  },
  link: {
    textDecoration: "none",
    color: "rgba(0,0,0,0.6)",
    fontWeight: "bold"
  }
};

City.propTypes = {
  city: PropTypes.object.isRequired
};

export default City;
