import React from "react";
import App from "./App";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Forecast from "./Forecast";

const Routes = () => (
  <Router>
    <div>
      <Route exact path="/" component={App} />
      <Route path="/forecast/:city" component={Forecast} />
    </div>
  </Router>
);
export default Routes;
